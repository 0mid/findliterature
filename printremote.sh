#! /bin/bash

# Copyright 2013 Omid Khanmohamadi ()
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

findliterature_dir=$(dirname $(readlink -f $0))
source ${findliterature_dir}/findliterature.config

usage(){
    echo -e "Usage:
$(basename "$0") filename \
[printer ($defaultprinter,-Pmch115,-Plw)] \
[host ($defaulthost)] \
[printoptions ($defaultprintoptions)]"
    exit 1
}

# call usage() function if filename not supplied
[[ $# -eq 0 ]] && usage

localfullfilename="$1"
filename=$(basename "${localfullfilename}")

# I settled on the following hack to escape (illegal characters in)
# the file name that's to be sent to the remote machine since down
# below I wanted to keep using the clean "here document" instead of
# tacking commands on one line and I couldn't find a way to have the
# local shell do its variable expansion and at the same time send
# non-literal double quotes " to the remote shell to protect the file
# name.
#
# http://stackoverflow.com/questions/5608112/escape-filenames-using-the-same-way-bash-do-it
remotefullfilename=$(printf '%q' "${remotedir}/${filename}")

if [[ -z $2 ]]; then
    printer=${defaultprinter}
else
    printer=$2
fi

if [[ -z $3 ]]; then
    remotehost=${defaulthost}
else
    remotehost=$3
fi

n=4			   # 4th argument onwards must become printoptions
if [[ -z ${!n} ]]; then	   # ${!n} becomes $4, by variable indirection
    printoptions=${defaultprintoptions}
else
    # http://stackoverflow.com/questions/6794809/how-to-exclude-bash-input-arguments-in
    printoptions="${@:$n}"
fi

# /bin/sh forces the following command to run in sh on remote
# regardless of the default shell on remote
ssh -t ${remotehost} /bin/sh "/bin/test -e ${remotefullfilename}" && \
    echo "File ${remotefullfilename} already exists on remote." || \
    scp "${localfullfilename}" "${remotehost}":"${remotefullfilename}"

# A "here document" is used below. N.B.: "${remotefullfilename}" does
# NOT work. The double quotes "" are sent to the remote shell as
# literal characters and are considered as part of the file name there
# on the remote machine, causing a "file not found" type error.
ssh ${remotehost} <<EOF
lpr ${printer} ${printoptions} \
${remotefullfilename}
EOF
