#!/bin/bash

# Copyright 2013 Omid Khanmohamadi ()
#
# This file is part of findliterature.
# 
# findliterature is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


# Watch ${literature_dir} for changes using inotifywait

# To run as a persistent background process and create a new file list, use
#
# nohup watchliterature.sh 2>&1 | tee /dev/null &
#
# To run as a persistent background process and reuse the old file list, use
#
# nohup watchliterature.sh --watch-only 2>&1 | tee /dev/null &

findliterature_dir=$(dirname "$(readlink -f "$0")")
source ${findliterature_dir}/findliterature.config

mkdir --parents "${filelist_dir}"

col_sep='@##@'		      # a string not likely to be part of filenames

appendfilename(){
    echo "${filename}" >>"${filelist}"
}

substitutefilename(){
    sed -i "s#${filename_old}#${filename}#" "${filelist}"
}

removefilename(){
    # The address (which appears before the command) filters which lines the
    # command is applied to. The options (which appear after the command)
    # are used to control the replacement applied. If we want to use
    # different delimiters for a regular expression in the address, we need
    # to start the address with a backslash. To understand the difference
    # between the address and the options, consider the output of the
    # command:
    #
    # sed '/timebomb/s/log/txt/' test.txt
    #
    # The address chooses only the line(s) containing the regular expression
    # 'timebomb', but the options tell the s command to replace the regular
    # expression 'log' with 'txt'.)
    sed -i "\#${filename}#d" "${filelist}"
}

buildfilelist(){
    find "${literature_dir}" \
	-type f \
	\! -path "${findexcludepath}" \
	>"${filelist}"
}

checkinotify(){
    inotifyprogram=inotifywait
    package2install=inotify-tools
    if ! ${isinstalled} "${inotifyprogram}" --install "${package2install}"; then
	exit 1
    fi
}

setupwatches(){
    checkinotify

    inotifywait --recursive --monitor --event create,move,delete \
	--excludei "${watchexcluderegex}" \
	--format "%e${col_sep}%w%f" "${literature_dir}" |
    while read line; do
	event=$(echo "${line}" | awk -F"${col_sep}" ' {print $1} ')
	filename=$(echo "${line}" | awk -F"${col_sep}" ' {print $2} ')
	case ${event^^} in		# ${event^^} is a bashism for UPPERCASEing
	    CREATE)
		echo "Created ${filename}"
		appendfilename
		;;
	    MOVED_FROM|MOVED_FROM,ISDIR)
		filename_old=${filename}
		continue
		;;
	    MOVED_TO|MOVED_TO,ISDIR)
		[[ ${filename_old} != "" ]] && \
		    # for ${filename_old} to be non-empty, MOVED_FROM must have
		# been triggered, which means file was moved within watched
		# dir's and in ${filelist} we substitute its new name
		# ${filename} for the old name ${filename_old}
		{ echo "Moved from ${filename_old} to ${filename}"
		    substitutefilename
		    filename_old=""
		} || \
		    # otherwise, file was moved to a watched dir from a
		# non-watched dir and we append its name to ${filelist}
		{ echo "Moved from non-watched dir to watched dir"
		    appendfilename
		}
		;;
	    DELETE)
		echo "Deleted ${filename}"
		removefilename
		;;
	esac
    done
}

params="$(getopt -o b::w:: --long build-only::,watch-only:: --name "$0" -- "$@")"
eval set -- "$params"

while true
do
    case "$1" in
	-b|--build-only)
	    buildfilelist
	    exit 0
	    ;;
	-w|--watch-only)
	    setupwatches
	    exit 0
	    ;;
	--)			# default
	    buildfilelist
	    setupwatches
	    exit 0
	    ;;
	*)
	    echo "Unknown option: $1" >&2
	    exit 1
	    ;;
    esac
done
