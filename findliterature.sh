#!/bin/bash

# Copyright 2013 Omid Khanmohamadi ()
#
# This file is part of findliterature.
#
# findliterature is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


# Find files in ${literature_dir} with ${pattern} in their name

# EXAMPLE. To find references containing 'strang' and 'applied' in
# their name:
#
# findliterature.sh strang applied
#
# EXAMPLE. To find references containing 'cloak' or 'cloaking' in
# their name:
#
# findliterature.sh 'cloak.*'
#
#   This regex pattern (note the dot in 'cloak.*') is necessary
#   because I decided to look for exact words and for that it is
#   necessary to use a regex, in particular the '\b' word boundary
#   indicator in the '-iregex' pattern. Previously, using a shell
#   pattern via '-iname',
#
#   findliterature.sh strang
#
#   would show results containing 'strange' in addition to those
#   containing only 'strang', an unwanted output and behavior.

findliterature_dir=$(dirname $(readlink -f $0))
source ${findliterature_dir}/findliterature.config

isCygwin() {
    [[ "$(uname -s)" =~ "CYGWIN" ]]
}

# Program to use for opening the file chosen by the user. "xdg-open"
# opens a file or URL in the user's preferred application; it opens
# the app in the "background", detached from terminal so that nohup
# and & are implied, and not needed explicitly.
program=xdg-open
if isCygwin; then program=cygstart; fi

# The complicated regular expression below has to do with the
# functionality to search for keywords in any order that they may
# appear in (full) file names, irrespective of the order that they are
# given as arguments to "findliterature.sh". In perl 5 regexp
# (activated in GNU grep via the switch -P), this can be done using
# "lookahead" assertions. For details on lookahead, see, e.g.,
# http://www.regular-expressions.info/lookaround.html. In short,
# lookahead says "match this position only if the following text
# matches (does not match) the given pattern." The syntax in Perl and
# similar regular expression implementations is (?= stuff ) for
# positive lookahead and (?! stuff) for negative lookahead. Note that
# lookaround regular expressions start with the symbol ? after which
# comes the direction (i.e., < for look-behind but nothing for
# lookahead) and then comes the polarity (i.e., = for positive, ! for
# negative. That is,
#
# look		(?
# behind	<
# positive	=
# negative	!
#
# http://www.johndcook.com/blog/2014/05/01/look-behind-regex/
# http://stackoverflow.com/questions/2219830/regular-expression-to-find-two-strings-anywhere-in-input
#
# EXAMPLE
#
: <<'BLOCKCOMMENT'

wordboundary='(?:\b|_)'
lookaheadprefix='(?='
lookaheadsuffix=')'
ngwildcard='.*?'
prefix=${lookaheadprefix}${ngwildcard}${wordboundary}
infix=${wordboundary}${lookaheadsuffix}
expfilename="this is__One_and, TWO-and three"
echo $expfilename | grep -i 'two one'
echo $expfilename | grep -i -P ${prefix}two${infix}${prefix}one${infix}${ngwildcard}

BLOCKCOMMENT

wordboundary='(?:\b|_)'		# treat underscore (_) as word boundary also
lookaheadprefix='(?='		# (? look ahead = positive
lookaheadsuffix=')'

# Note that with '-iregex' (or '-regex') the match is on the whole
# path, not just the base name of the file. For example, to match a
# file named './fubar3', you can use the regular expression '.*bar.'
# or '.*b.*3', but not 'f.*r3'. In other words, a leading and trailing
# '.*' (as we have in prefix and suffix) are crucial for the pattern
# to match anything.
ngwildcard='.*?'		# non-greedy wildcard

prefix=${lookaheadprefix}${ngwildcard}${wordboundary}
infix=${wordboundary}${lookaheadsuffix}

# Double-quoting $@ or ${array[@]} has a special meaning. "$@" expands
# to a list of words, with each positional parameter's value being one
# word. Likewise, "${array[@]}" expands to a list of words, one per
# array element. When dealing with the positional parameters or with
# the contents of an array as a list of words, always use the
# double-quoted syntax.
#
# Double-quoting $* or ${array[*]} results in one word which is the
# concatenation of all the positional parameters (or array elements)
# with the first character of IFS between them. This is similar to the
# join function in some other languages, although the fact that you
# can only have a single join character can sometimes be a crippling
# limitation.
#
# http://mywiki.wooledge.org/Quotes
#
# 'for arg' is equivalent to 'for arg in "$@"'.
#
# http://stackoverflow.com/questions/255898/how-to-iterate-over-arguments-in-bash-script

arguments_str="$@"

# ---------- BEGINNING OF FUNCTION DEFINITIONS ----------

formpattern(){
    pattern=""
    IFS=' ' read -a arguments <<<"${arguments_str}"
    for arg in "${arguments[@]}"
    do
	# '+=' does NOT work in sh, hence the '#!/bin/bash' "shebang" line
	# at the top of this script
	pattern+=${prefix}${arg}${infix}
   done
   pattern+=${ngwildcard}
   arguments=${arguments_str}
}

search4pattern(){
    [[ -f "${filelist}" ]] || bash ${findliterature_dir}/watchliterature.sh -b
    echo "grep -i -P ${pattern} -- ${filelist} | sort -q"

    # Set internal field separator (IFS) to "new line at the end of a
    # line" (regexp $'\n') so that in the rest of the script each file
    # name as a whole (as opposed to each space-separated word in a
    # file name) is stored in a separate array element.
    IFS=$'\n'

    # Outer-most parentheses create an array
    literature=( $(grep --ignore-case --perl-regexp \
	"${pattern}" -- "${filelist}" | sort --unique) )

    # [[ does NOT perform word splitting or glob expansion so the
    # variable can be left unqouted.
    if [[ -z ${literature[*]} ]]; then # exit if nothing found
	# tput setaf 1: set terminal foreground color to 1 (red)
	#
	# Black 0 Red 1 Green 2 Yellow 3 Blue 4 Magenta 5 Cyan 6
	# White 7
	#
	# tput sgr0: reset all terminal attributes
	echo -e "\n$(tput setaf 1)NO MATCHES FOUND.$(tput sgr0) \
Consider generalizing your pattern by using term.* instead of term \
(on command line with single quotes, 'term.*')"
	getnewarguments
	formpattern
	search4pattern
    fi
}

formfilename(){
    refl=${refn:0:1}		# get first character of $refn

    # Double bracket operator "[[ ]]" supports regular expressions via
    # the "=~" operator.

    # if the first character is a number, keep all characters.
    if [[ ${refl} =~ [0-9] ]]; then
	# double parentheses "(())" do integer arithmetic in bash
	filename="${literature[((${refn} - 1))]}"

    # otherwise, remove first character from $refn.
    else

	refn=${refn#?}
	[ ${refl} != q ] && filename=( "${literature[((${refn} - 1))]}" )
    fi
}

runprogram(){
    # To save a command in a variable, we need a way to maintain each
    # argument as a separate word, even if that argument contains spaces,
    # such as spaces in a file name argument. Quotes won't do it, but an
    # array will. To achieve this, put each argument in a separate element
    # of an array.
    #
    # http://mywiki.wooledge.org/BashFAQ/050
    echo "
${program}" "${filename}"
    "${program}" "${filename}" >/dev/null 2>&1
}

copy2clipboard(){
    local clipboardprogram package2install
    clipboardprogram=xclip
    package2install=${clipboardprogram}
    if ${isinstalled} "${clipboardprogram}" --install "${package2install}"; then
	echo "${filename}" | \
	tr --delete '\n' | \
	${clipboardprogram} -selection clipboard && \
	echo -e "\n$(tput setaf 2)Full filename (below) copied to clipboard.
${filename}$(tput sgr0)"
    fi
}

movefile(){
    read -e -p "
Move to (Enter to cancel): " -i "${filename}" newfilename

    if [[ ${newfilename} == ${filename} ]]; then # return if no chng made
	return 0
    fi

    [[ $(dirname "${newfilename}") != $(dirname "${filename}") ]] && \
	mkdir --parents $(dirname "${newfilename}")

    [[ ${newfilename} != "" ]] && \
	[[ ${newfilename} != ${filename} ]] && \
	mv --interactive "${filename}" "${newfilename}" && \
        if isCygwin; then
            # no inotify here; manually force a run of
            # watchliterature.sh to rebuild ${literaturefilelist}.
            bash ${findliterature_dir}/watchliterature.sh --build-only > "${tmpfile}" 2>&1 &
        else
	    showprogressbar
        fi
}

removefile(){
    rm --interactive "${filename}"
    if [[ -f ${filename} ]]; then # if file still there, user entered 'n'
	return 0
    fi
    showprogressbar
}

urlencodefilename(){
    # A safe way to urlencode is to encode every single byte, even
    # those that would've been allowed. This is done by first
    # converting every byte to its 2-byte hex code using hexdump and
    # then adding the prefix "%" to each 2-byte hex code pair using
    # sed.
    #
    # https://stackoverflow.com/a/7506695
    filenameurlencoded=$(echo -ne "${filename}" | \
	hexdump -v -e '/1 "%02x"' | \
	sed 's/\(..\)/%\1/g')
}

attachfilethunderbird(){
    # Syntax of -compose command line option: double-quotes enclose
    # full comma-separated list of arguments passed to -compose,
    # whereas single quotes are used to group items for the same
    # argument. For attaching from command line to work properly using
    # Thunderbird, the filename must be URL encoded, otherwise when
    # there are 'illegal' characters (such as ",") in it Thunderbird
    # won't attach it when called from command line (GUI has no
    # problem; this must be a bug in their wrapper script). Example
    # calling syntax:
    #
    # thunderbird -compose "to='name1@example.com,name2@example.com',cc='name3@example.com',subject='Email Subject',body='Email Body',attachment='file:///$HOME/urlencodedfilename'"

    local filenamenopath filenamenoext
    local emailsubject emailbody emailattachment

    local clipboardprogram package2install
    emailprogram=thunderbird
    package2install=${emailprogram}
    if ! ${isinstalled} "${emailprogram}" --install "${package2install}"; then
	return 1
    fi

    filenamenopath="$(basename ${filename})"
    filenamenoext="${filenamenopath%.*}"
    emailsubject="${filenamenoext}"
    emailbody="\"${filenamenopath}\" is attached.
"
    urlencodefilename
    emailattachment="file:///${filenameurlencoded}"

    ${emailprogram} -compose "subject='${emailsubject}',body='${emailbody}',attachment='${emailattachment}'"
}

printfile(){
    command -v printremote.sh >/dev/null 2>&1 && \
	{ defaulthost="math_hilbert"
	defaultprinter="-Pmch406"
	defaultprinteroptions="-o sides=two-sided-long-edge -o page-ranges=1-"
	printoptions="${defaultprinter} ${defaulthost} ${defaultprinteroptions}"
	read -e -p "
printremote.sh options (e.g., -P{mch406,mch115,lw}; -o page-ranges=1-7,12/1- means all pages):" \
    -i ${printoptions} newprintoptions_str
	IFS=' ' read -a newprintoptions <<<${newprintoptions_str}

	# For some reason the following that is also supposed to
	# convert a string into an array doesn't work.
	#
	# newprintoptions=(${newprintoptions_str})

	echo -e "\nprintremote.sh \"${filename}\" ${newprintoptions_str}"

	# printremote.sh expects arguments in the following order:
	# printremote.sh ${filename} ${printer} ${remotehost}
	# ${printeroptions}
	printremote.sh \
	    "${filename}" \
	    ${newprintoptions[0]} \
	    ${newprintoptions[1]} \
	    "${newprintoptions[@]:2}" # all elements from 2 (i.e., 3rd) onward

    } || \
	# Note that if any command in the block above (including
	# printremote.sh) exits abnormally the following "or" block
	# will be excuted. The message printed does not make sense in
	# those cases, of course.

	# ending semicolon (;) necessary if all commands on one line
    { echo "printremote.sh not found."; }
}

showprogressbar(){
    # this fake progress bar is to enforce sleep which is necessary so
    # that inotifywait has time to see the changes and update the
    # ${filelist}
    echo -e "\nUpdating filelist (${filelist})"
    echo -ne '[====>               ]\r'
    sleep .5s
    echo -ne '[=======>            ]\r'
    sleep .5s
    echo -ne '[===========>        ]\r'
    sleep .5s
    echo -ne '[===============>    ]\r'
    sleep .5s
    echo -ne '[===================>]\r'
    sleep .1s
    echo -ne '[DONE]                \r'
    echo -ne '\n\n'
}

getnewarguments(){
    # use previous ${arguments} as default
    read -e -p "
Enter new search arguments (e.g., cloak.* for cloak, cloaking, cloaks, etc): " -i "${arguments}" arguments_str
}

rebuildfilelist(){
    local tmpfile tmpfifo msg2waitfor
    tmpfile=$(mktemp --tmpdir=/tmp watchliterature_stdouterr.XXXXXX)
    tmpfifo=$(mktemp --tmpdir=/tmp --dry-run watchliterature_fifo.XXXXXX)
    msg2waitfor="Watches established."

    if isCygwin; then
        cp ${filelist_dir}/literaturefilelist.{txt,bak}
        bash ${findliterature_dir}/watchliterature.sh --build-only > "${tmpfile}" 2>&1 &
        return
    fi

    killall watchliterature.sh > /dev/null 2>&1 && sleep 1s
    cp ${filelist_dir}/literaturefilelist.{txt,bak}
    nohup watchliterature.sh > "${tmpfile}" 2>&1 &

    echo "Establishing watches in ${literature_dir} ..."

    # You can get tail to exit by sending it a signal like SIGTERM.
    # The challenge is reliably knowing two things in the same place
    # in code: tail's PID and whether grep has exited. The second
    # pipeline stage knows when grep has exited, so getting the PID to
    # the second stage would solve the problem. The first pipeline
    # stage can easily and reliably get tail's PID by backgrounding
    # tail and reading $!. If the first pipeline stage could reliably
    # communicate the PID to the second pipeline stage the problem
    # would be solved. Unfortunately, the first pipeline stage can't
    # simply set a variable to be read by the second pipeline stage
    # because the two stages are in separate execution environments
    # (subshells). The first stage could send the PID to the second
    # stage via the pipe by echoing the PID, but this string will get
    # mixed with tail's output. If the echo is interleaved with tail's
    # output, it won't be possible to reliably distinguish tail's
    # output from the PID string, even with a complex escaping scheme.
    # The second stage could use pgrep, but that would be unreliable
    # (you might match the wrong process) and non-POSIX
    # (non-portable). So, instead you can have the second pipeline
    # stage notify the first pipeline stage via a FIFO that grep has
    # exited and let the first stage kill tail.
    #
    # https://superuser.com/a/548193/86394
    mkfifo "${tmpfifo}" || exit 1
    {
	# run tail in the background so that the shell can
	# kill tail when notified that grep has exited
	tail -f "${tmpfile}" &
	# remember tail's PID
	tailpid=$!

	# wait for notification that grep has exited. (N.B. reading
	# from or writing to a FIFO is a blocking operation and that's
	# crucial here. We don't want to kill tail in this pipeline
	# stage until grep has seen ${msg2waitfor} in the next
	# pipeline stage.)
	read foo <${tmpfifo}

	# grep has exited, time to kill!
	kill "${tailpid}"
    } | {
	grep --max-count=1 "${msg2waitfor}" &&

	# notify the first pipeline stage that grep is done
	echo >${tmpfifo}
    }
    # clean up
    rm "${tmpfifo}"

    showprogressbar
}

# ---------- END OF FUNCTION DEFINITIONS ----------

formpattern
search4pattern

while true; do
    echo

# Print out all elements of the array ${literature}
#
# less --no-init (-X) keeps page from less in terminal after exit
#
# http://stackoverflow.com/questions/6977781/xterm-keep-page-from-less-in-terminal-after-exit
    echo "${literature[*]}" | cat -n |\
    less --quit-at-eof --quit-if-one-screen \
	--ignore-case --SILENT --hilite-unread --no-init

    read -p "
Enter q to QUIT | \
n for NEW search | \
b to reBUILD filelist | \
c# (e.g, c12) to CLIPBOARD filename | \
p# to PRINT | \
m# to MOVE (mv) | \
r# to REMOVE (rm) | \
a# to ATTACH (Thunderbird) | \
# (e.g., 12) to OPEN: " refn

    formfilename		# form $filename to be used by $command

    # Note: As far as I found, quoting does NOT play nicely with
    # wildcards; e.g., "c*" or 'c*' will NOT work. Double quoting the
    # parameter (here ${refl}) in a case construct is not necessary.
    case ${refl} in
	q)			# QUIT
	    unset IFS && break
	    ;;

	n)			# NEW SEARCH
	    getnewarguments
	    formpattern
	    search4pattern
	    ;;

	b)			# reBUILD filelist
	    rebuildfilelist
	    search4pattern	# update search results after filelist rebuild
	    ;;

	c)			# COPY FULL FILENAME to CLIPBOARD
	    copy2clipboard
	    ;;

	p)			# PRINT
	    printfile
	    ;;

	m)			# MOVE
	    movefile
	    search4pattern	# update search results after possible move
	    ;;

	r)			# REMOVE
	    removefile
	    search4pattern	# update search results after possible remove
	    ;;

	a)			# ATTACH
	    attachfilethunderbird
	    copy2clipboard
	    ;;
	*)			# OPEN
	    runprogram
	    copy2clipboard
	    ;;
    esac

done

exit 0
