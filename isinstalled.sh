#!/bin/bash

# Copyright 2015 Omid Khanmohamadi <>
#
# isinstalled.sh is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

params="$(getopt -o i: --long install: --name "$0" -- "$@")"
eval set -- "$params"

while true
do
    case "$1" in
	-i|--install)
	    package2install=$2
	    shift 2
	    ;;
	--)
	    shift
	    break
	    ;;
	*)
	    echo "Unknown option: $1" >&2
	    exit 1
	    ;;
    esac
done

program2check=$1

if command -v "${program2check}" >/dev/null 2>&1; then
    exit 0
else
    echo -e >&2 "\n$(tput setaf 1)${program2check} is needed but it was not found. \
Install ${program2check} (e.g. using, sudo apt-get install ${package2install}) \
and try again.$(tput sgr0)"
    exit 1
fi
